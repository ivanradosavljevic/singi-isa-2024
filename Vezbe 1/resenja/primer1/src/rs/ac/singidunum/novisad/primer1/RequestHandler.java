package rs.ac.singidunum.novisad.primer1;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class RequestHandler implements Runnable {
	private Socket socket;

	public RequestHandler(Socket socket) {
		super();
		this.socket = socket;
	}

	@Override
	public void run() {
		try {
			PrintWriter output = new PrintWriter(socket.getOutputStream());
			BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			System.out.println(input.readLine());
	
			Thread.sleep(5000);
	
			output.println("odgovor");
			output.flush();
	
			input.close();
			output.close();
			socket.close();
		} catch (Exception e) {
		}
	}

}
