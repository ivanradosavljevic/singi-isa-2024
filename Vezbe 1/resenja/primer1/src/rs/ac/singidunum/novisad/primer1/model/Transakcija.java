package rs.ac.singidunum.novisad.primer1.model;

public class Transakcija {
	private Racun racunUplatioca;
	private Racun racunPrimaoca;
	private double iznos;

	public Transakcija() {
		super();
	}

	public Transakcija(Racun racunUplatioca, Racun racunPrimaoca, double iznos) {
		super();
		this.racunUplatioca = racunUplatioca;
		this.racunPrimaoca = racunPrimaoca;
		this.iznos = iznos;
	}

	public Racun getRacunUplatioca() {
		return racunUplatioca;
	}

	public void setRacunUplatioca(Racun racunUplatioca) {
		this.racunUplatioca = racunUplatioca;
	}

	public Racun getRacunPrimaoca() {
		return racunPrimaoca;
	}

	public void setRacunPrimaoca(Racun racunPrimaoca) {
		this.racunPrimaoca = racunPrimaoca;
	}

	public double getIznos() {
		return iznos;
	}

	public void setIznos(double iznos) {
		this.iznos = iznos;
	}
}
