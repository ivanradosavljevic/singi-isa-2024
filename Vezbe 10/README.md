# Vežbe 10

**Cilj vežbi:** Realizovati asinhronu razmenu poruka izmedju dve ili više Spring boot aplikacija.

1. Napraviti aplikaciju za vršenje evidencije uplate i isplate novca sa računa. Za svaku uplatu i isplatu napraviti po jedan zapis o evidenciji uplate i isplate, zapis treba da sadrži račun sa kojeg je izvršena uplata, račun na koji se vrši uplata, iznos i datum i vreme uplate ili isplate. Ovaj zapis proslediti aplikaciji za čuvanje istorje transakcija. Ova aplikacija primenljenu tranksakciju zapisuje u odvojenu bazu podataka.
2. Napraviti aplikacju za kupovinu. Svaka kupovina generiše asinhronu poruku koju je potrebno poslati aplikaciji za evidencije o uplatama i isplatama. Nakon evidentiranja ispalte aplikacija za evidencije vraća odgovor aplikaciji za kupovinu koji treba da sadrži podatka o tome da li je prenos sredstava uspešan.
___
### Dodatne napomene:
* Dokumentacij za Spring Data JPA: https://docs.spring.io/spring-boot/docs/current-SNAPSHOT/reference/html/messaging.html#messaging
* Dokumentacija za AOP u Spring radnom okviru: https://docs.spring.io/spring-framework/docs/current/reference/html/core.html#aop
___
