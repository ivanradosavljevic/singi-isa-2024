package rs.ac.singidunum.novisad.app.configuration;

import org.apache.activemq.artemis.jms.client.ActiveMQQueue;
import org.apache.activemq.artemis.jms.client.ActiveMQTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import jakarta.jms.Queue;
import jakarta.jms.Topic;

@Configuration
public class ActiveMQConfiguration {
	@Bean
	public Queue logQueue() {
		return new ActiveMQQueue("messages");
	}
	
	@Bean
	public Topic logTopic() {
		return new ActiveMQTopic("messagesTopic");
	}
}
