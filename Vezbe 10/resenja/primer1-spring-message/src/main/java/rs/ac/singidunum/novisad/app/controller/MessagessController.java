package rs.ac.singidunum.novisad.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jakarta.jms.Queue;
import jakarta.jms.Topic;
import rs.ac.singidunum.novisad.app.dto.MessageDTO;



@Controller
@RequestMapping("/api")
public class MessagessController {
	@Autowired
	Queue messageQueue;
	
	@Autowired
	Topic messageTopic;
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@RequestMapping(path="/message", method = RequestMethod.POST)
	public ResponseEntity<MessageDTO> sendMessage(@RequestBody MessageDTO message) {
		jmsTemplate.convertAndSend(messageQueue, message);
		return new ResponseEntity<MessageDTO>(message, HttpStatus.OK);
	}
	
	@RequestMapping(path="/messageTopic", method = RequestMethod.POST)
	public ResponseEntity<MessageDTO> sendMessageToAll(@RequestBody MessageDTO message) {
		jmsTemplate.convertAndSend(messageTopic, message);
		return new ResponseEntity<MessageDTO>(message, HttpStatus.OK);
	}
}
