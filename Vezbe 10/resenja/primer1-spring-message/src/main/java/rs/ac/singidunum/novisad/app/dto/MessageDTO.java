package rs.ac.singidunum.novisad.app.dto;

import java.io.Serializable;
import java.time.LocalDate;

public class MessageDTO implements Serializable {
	private static final long serialVersionUID = 8725139183460979463L;
	private String title;
	private String content;
	private LocalDate dateTime;
	
	public MessageDTO() {
		super();
	}
	public MessageDTO(String title, String content, LocalDate dateTime) {
		super();
		this.title = title;
		this.content = content;
		this.dateTime = dateTime;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public LocalDate getDateTime() {
		return dateTime;
	}
	public void setDateTime(LocalDate dateTime) {
		this.dateTime = dateTime;
	}
}
