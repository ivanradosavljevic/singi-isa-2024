package rs.ac.singidunum.novisad.app.listener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import rs.ac.singidunum.novisad.app.dto.MessageDTO;

@Component
public class MessageListener {

	@JmsListener(destination = "messages")
	public void onMessage(MessageDTO message) {
		System.out.println(message);
	}
}
