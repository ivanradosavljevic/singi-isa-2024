package rs.ac.singidunum.novisad.app.listener;

import java.time.LocalDate;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import rs.ac.singidunum.novisad.app.dto.MessageDTO;

@Component
public class MessageListener {

	@JmsListener(destination = "messagesTopic")
	@SendTo("confirmation")
	public MessageDTO onMessage(MessageDTO message) {
		System.out.println(message);
		return new MessageDTO("RESPONSE", "RESPONSE CONTENT", LocalDate.now());
	}
}
