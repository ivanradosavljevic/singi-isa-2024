# Vežbe 10

**Cilj vežbi:** Upotrebom Spring Cloud biblioteka realizovati mikroservisnu arhitekturu.

1. Napraviti apliakciju za registraciju mikroservise.
2. Napraviti mikroservis koji se registruje na aplikaciju za registraciju mikroservisa.
3. Napraviti gateway aplikaciju koja preusmerava saobraćaj do mikroservisa.
___
### Dodatne napomene:
* Dokumentacij za Spring Cloud Netflix: https://spring.io/projects/spring-cloud-netflix
* Dokumentacija za Spring Cloud Gateway: https://spring.io/projects/spring-cloud-gateway
___
