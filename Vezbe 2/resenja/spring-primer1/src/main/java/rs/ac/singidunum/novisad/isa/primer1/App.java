package rs.ac.singidunum.novisad.isa.primer1;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import rs.ac.singidunum.novisad.isa.primer1.view.RacunView;

public class App {
	public static void main(String []args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		RacunView rv = context.getBean(RacunView.class);
		
		rv.show();
		
		context.close();
	}
}
