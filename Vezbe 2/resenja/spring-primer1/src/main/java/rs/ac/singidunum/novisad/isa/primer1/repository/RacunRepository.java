package rs.ac.singidunum.novisad.isa.primer1.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import rs.ac.singidunum.novisad.isa.primer1.model.Racun;

public class RacunRepository {
	private HashMap<String, Racun> racuni = new HashMap<String, Racun>();

	public RacunRepository() {
		super();
		racuni.put("1234567", new Racun("Korisnik 1", "1234567", 10000, true));
		racuni.put("1234569", new Racun("Korisnik 2", "1234569", 30000, true));
		racuni.put("1234510", new Racun("Korisnik 2", "1234510", 15000, true));
	}

	public List<Racun> findAll() {
		return new ArrayList<Racun>(racuni.values());
	}

	public Racun findOne(String brojRacuna) {
		return racuni.get(brojRacuna);
	}

	public boolean save(Racun noviRacun) {
		if (racuni.containsKey(noviRacun.getBrojRacuna())) {
			return false;
		}
		racuni.put(noviRacun.getBrojRacuna(), noviRacun);
		return true;
	}

	public boolean update(Racun racun) {
		if (!racuni.containsKey(racun.getBrojRacuna())) {
			return false;
		}
		racuni.replace(racun.getBrojRacuna(), racun);
		return true;
	}

	public boolean delete(String brojRacuna) {
		if (!racuni.containsKey(brojRacuna)) {
			return false;
		}
		racuni.remove(brojRacuna);
		return true;
	}

	public boolean delete(Racun racun) {
		return delete(racun.getBrojRacuna());
	}

	public List<Racun> findByStanjeBetween(double min, double max) {
		ArrayList<Racun> filtriraniRacuni = new ArrayList<Racun>();

		for (Racun r : racuni.values()) {
			if (r.getStanje() <= max && r.getStanje() >= min) {
				filtriraniRacuni.add(r);
			}
		}

		return filtriraniRacuni;
	}
}
