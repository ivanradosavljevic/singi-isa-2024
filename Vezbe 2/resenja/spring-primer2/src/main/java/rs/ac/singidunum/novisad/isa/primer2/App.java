package rs.ac.singidunum.novisad.isa.primer2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import rs.ac.singidunum.novisad.isa.primer2.configuration.AppConfiguration;
import rs.ac.singidunum.novisad.isa.primer2.view.RacunView;

public class App {
	public static void main(String []args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class);
//		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		
		RacunView rv = context.getBean(RacunView.class);
		rv.show();
		
		context.close();
	}
}
