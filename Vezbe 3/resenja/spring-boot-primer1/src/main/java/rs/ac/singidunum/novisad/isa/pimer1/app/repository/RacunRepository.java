package rs.ac.singidunum.novisad.isa.pimer1.app.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import rs.ac.singidunum.novisad.isa.pimer1.app.model.Racun;

@Repository
public class RacunRepository {
	KorisnikRepository korisnikRepository;
	
	private HashMap<String, Racun> racuni = new HashMap<String, Racun>();

	public RacunRepository(KorisnikRepository korisnikRepository) {
		super();
		this.korisnikRepository = korisnikRepository;
		Racun racun1 = new Racun(korisnikRepository.findOne(1l), "1234567", 10000, true);
		Racun racun2 = new Racun(korisnikRepository.findOne(2l), "1234569", 30000, true);
		Racun racun3 = new Racun(korisnikRepository.findOne(2l), "1234510", 15000, true);
		
		korisnikRepository.findOne(1l).getRacuni().add(racun1);
		korisnikRepository.findOne(2l).getRacuni().add(racun2);
		korisnikRepository.findOne(2l).getRacuni().add(racun3);
		
		
		racuni.put("1234567", racun1);
		racuni.put("1234569", racun2);
		racuni.put("1234510", racun3);
	}

	public List<Racun> findAll() {
		return new ArrayList<Racun>(racuni.values());
	}

	public Racun findOne(String brojRacuna) {
		return racuni.get(brojRacuna);
	}

	public boolean save(Racun noviRacun) {
		if (racuni.containsKey(noviRacun.getBrojRacuna())) {
			return false;
		}
		racuni.put(noviRacun.getBrojRacuna(), noviRacun);
		return true;
	}

	public boolean update(Racun racun) {
		if (!racuni.containsKey(racun.getBrojRacuna())) {
			return false;
		}
		racuni.replace(racun.getBrojRacuna(), racun);
		return true;
	}

	public boolean delete(String brojRacuna) {
		if (!racuni.containsKey(brojRacuna)) {
			return false;
		}
		racuni.remove(brojRacuna);
		return true;
	}

	public boolean delete(Racun racun) {
		return delete(racun.getBrojRacuna());
	}

	public List<Racun> findByStanjeBetween(double min, double max) {
		System.out.println(this.korisnikRepository);
		ArrayList<Racun> filtriraniRacuni = new ArrayList<Racun>();

		for (Racun r : racuni.values()) {
			if (r.getStanje() <= max && r.getStanje() >= min) {
				filtriraniRacuni.add(r);
			}
		}

		return filtriraniRacuni;
	}

	public KorisnikRepository getKorisnikRepository() {
		return korisnikRepository;
	}

	public void setKorisnikRepository(KorisnikRepository korisnikRepository) {
		this.korisnikRepository = korisnikRepository;
	}
}
