package rs.ac.singidunum.novisad.isa.pimer1.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.isa.pimer1.app.model.Korisnik;
import rs.ac.singidunum.novisad.isa.pimer1.app.repository.KorisnikRepository;

@Service
public class KorisnikService {
	@Autowired
	private KorisnikRepository korisnikRepository;

	public KorisnikService() {
		super();
	}

	public KorisnikService(KorisnikRepository korisnikRepository) {
		super();
		this.korisnikRepository = korisnikRepository;
	}

	public KorisnikRepository getKorisnikRepository() {
		return korisnikRepository;
	}

	public void setKorisnikRepository(KorisnikRepository korisnikRepository) {
		this.korisnikRepository = korisnikRepository;
	}

	public List<Korisnik> findAll() {
		return korisnikRepository.findAll();
	}

	public Korisnik findOne(Long id) {
		return korisnikRepository.findOne(id);
	}

	public boolean save(Korisnik noviKorisnik) {
		return korisnikRepository.save(noviKorisnik);
	}

	public boolean update(Korisnik korisnik) {
		return korisnikRepository.update(korisnik);
	}

	public boolean delete(Long id) {
		return korisnikRepository.delete(id);
	}

	public boolean delete(Korisnik korisnik) {
		return korisnikRepository.delete(korisnik);
	}
}
