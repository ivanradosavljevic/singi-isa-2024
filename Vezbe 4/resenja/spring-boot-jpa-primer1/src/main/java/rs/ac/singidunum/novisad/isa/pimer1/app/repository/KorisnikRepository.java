package rs.ac.singidunum.novisad.isa.pimer1.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.novisad.isa.pimer1.app.model.Korisnik;

@Repository
public interface KorisnikRepository extends CrudRepository<Korisnik, Long> {
}
