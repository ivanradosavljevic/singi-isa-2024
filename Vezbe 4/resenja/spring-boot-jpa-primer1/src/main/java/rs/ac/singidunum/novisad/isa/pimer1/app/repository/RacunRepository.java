package rs.ac.singidunum.novisad.isa.pimer1.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import rs.ac.singidunum.novisad.isa.pimer1.app.model.Racun;

public interface RacunRepository extends CrudRepository<Racun, String> {
	List<Racun> findByStanjeBetween(double min, double max);
	
	@Query("SELECT r FROM Racun r WHERE r.stanje > :min AND r.stanje < :max")
	List<Racun> pronadjiRacuneIzmedjuMinIMax(double min, double max);
}
