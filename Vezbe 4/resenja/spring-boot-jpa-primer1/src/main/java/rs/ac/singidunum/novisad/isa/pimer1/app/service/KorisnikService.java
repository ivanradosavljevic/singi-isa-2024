package rs.ac.singidunum.novisad.isa.pimer1.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.isa.pimer1.app.model.Korisnik;
import rs.ac.singidunum.novisad.isa.pimer1.app.repository.KorisnikRepository;

@Service
public class KorisnikService {
	@Autowired
	private KorisnikRepository korisnikRepository;
	
	public Iterable<Korisnik> findAll() {
		return this.korisnikRepository.findAll();
	}
	
	public Optional<Korisnik> findById(Long id) {
		return this.korisnikRepository.findById(id);
	}
	
	public Korisnik save(Korisnik korisnik) {
		return this.korisnikRepository.save(korisnik);
	}
	
	public void delete(Korisnik korisnik) {
		this.korisnikRepository.delete(korisnik);
	}
	
	public void delete(Long id) {
		this.korisnikRepository.deleteById(id);
	}
} 
