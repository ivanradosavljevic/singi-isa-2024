package rs.ac.singidunum.novisad.isa.pimer1.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.isa.pimer1.app.model.Racun;
import rs.ac.singidunum.novisad.isa.pimer1.app.repository.RacunRepository;

@Service
public class RacunService {
	@Autowired
	private RacunRepository racunRepository;

	public RacunService() {
		super();
	}

	public RacunService(RacunRepository racunRepository) {
		super();
		this.racunRepository = racunRepository;
	}

	public RacunRepository getRacunRepository() {
		return racunRepository;
	}

	public void setRacunRepository(RacunRepository racunRepository) {
		this.racunRepository = racunRepository;
	}

	public Iterable<Racun> findAll() {
		return racunRepository.findAll();
	}

	public Optional<Racun> findOne(String brojRacuna) {
		return racunRepository.findById(brojRacuna);
	}

	public Racun save(Racun noviRacun) {
		return racunRepository.save(noviRacun);
	}

	public Racun update(Racun racun) {
		if(racunRepository.findById(racun.getBrojRacuna()).isPresent()) {
			return racunRepository.save(racun);
		}
		return null;	
	}

	public void delete(String brojRacuna) {
		racunRepository.deleteById(brojRacuna);
	}

	public void delete(Racun racun) {
		racunRepository.delete(racun);
	}

	public List<Racun> findByStanjeBetween(double min, double max) {
		return racunRepository.findByStanjeBetween(min, max);
	}

	public boolean blokiraj(String brojRacuna) {
		Racun r = racunRepository.findById(brojRacuna).orElse(null);
		if (r == null) {
			return false;
		}

		r.setBlokiran(true);
		r.setStanje(0);
		racunRepository.save(r);
		return true;
	}

	public boolean blokiraj(Racun racun) {
		return this.blokiraj(racun.getBrojRacuna());
	}
}
