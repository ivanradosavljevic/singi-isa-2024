# Vežbe 7
**Cilj vežbi:** Implementirati Spring Boot aplikaciju sa podrškom za za transakcije, zaključavanje i nasleđivanje.

1. Proširiti pojam korisnika uvođenjem pojma administrativnog korisnika, administrativni korisnik je korisnik koji dodatno sadrži podatak korisničkom imenu, lozinci i pravima pristupa.
2. U servis dodati metodu za dodavanje novog korisnika koja prilikom kreiranja korisnika kreira i račun. Kreiranje novog korisnika se ne sme desiti ako ne dođe do ispravnog kreiranja računa.
3. Sprečiti neispravan pristup podacima prilikom čitanja i menjanja transakcija.
___
### Dodatne napomene:
* Dokumentacij za Spring Data JPA: https://docs.spring.io/spring-data/jpa/docs/current/reference/html.
___
