package rs.ac.singidunum.novisad.isa.pimer3.app.model;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;

@Entity
public class AdministrativniKorisnik extends Korisnik {
	private String korisnickoIme;
	private String lozinka;
	
	@OneToMany(mappedBy = "administrator")
	private List<DodeljenoPravoPristupa> dodeljenaPravapristupa = new ArrayList<DodeljenoPravoPristupa>();

	public AdministrativniKorisnik() {
		super();
	}

	public AdministrativniKorisnik(Long id, String ime, String prezime) {
		super(id, ime, prezime);
	}

	public AdministrativniKorisnik(Long id, String ime, String prezime, String korisnickoIme, String lozinka,
			List<DodeljenoPravoPristupa> dodeljenaPravapristupa) {
		super(id, ime, prezime);
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.dodeljenaPravapristupa = dodeljenaPravapristupa;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public List<DodeljenoPravoPristupa> getDodeljenaPravapristupa() {
		return dodeljenaPravapristupa;
	}

	public void setDodeljenaPravapristupa(List<DodeljenoPravoPristupa> dodeljenaPravapristupa) {
		this.dodeljenaPravapristupa = dodeljenaPravapristupa;
	}
}
