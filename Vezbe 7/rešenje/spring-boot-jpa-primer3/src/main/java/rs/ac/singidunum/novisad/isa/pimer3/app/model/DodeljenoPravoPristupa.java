package rs.ac.singidunum.novisad.isa.pimer3.app.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
public class DodeljenoPravoPristupa {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	private AdministrativniKorisnik administrator;
	
	@ManyToOne
	private PravoPristupa dodeljenoPravo;

	public DodeljenoPravoPristupa() {
		super();
	}

	public DodeljenoPravoPristupa(Long id, AdministrativniKorisnik administrator, PravoPristupa dodeljenoPravo) {
		super();
		this.id = id;
		this.administrator = administrator;
		this.dodeljenoPravo = dodeljenoPravo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AdministrativniKorisnik getAdministrator() {
		return administrator;
	}

	public void setAdministrator(AdministrativniKorisnik administrator) {
		this.administrator = administrator;
	}

	public PravoPristupa getDodeljenoPravo() {
		return dodeljenoPravo;
	}

	public void setDodeljenoPravo(PravoPristupa dodeljenoPravo) {
		this.dodeljenoPravo = dodeljenoPravo;
	}
}
