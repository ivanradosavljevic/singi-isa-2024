package rs.ac.singidunum.novisad.isa.pimer3.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.novisad.isa.pimer3.app.model.Korisnik;

@Repository
public interface KorisnikRepository extends CrudRepository<Korisnik, Long> {
}
