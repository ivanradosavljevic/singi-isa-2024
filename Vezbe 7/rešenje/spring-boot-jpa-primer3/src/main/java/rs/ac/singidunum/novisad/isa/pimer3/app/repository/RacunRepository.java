package rs.ac.singidunum.novisad.isa.pimer3.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import jakarta.persistence.LockModeType;
import rs.ac.singidunum.novisad.isa.pimer3.app.model.Racun;

public interface RacunRepository extends CrudRepository<Racun, String> {
	@Lock(LockModeType.OPTIMISTIC)
	@Override
	<S extends Racun> S save(S entity);
	
	List<Racun> findByStanjeBetween(double min, double max);
	
	@Query("SELECT r FROM Racun r WHERE r.stanje > :min AND r.stanje < :max")
	List<Racun> pronadjiRacuneIzmedjuMinIMax(double min, double max);
}
