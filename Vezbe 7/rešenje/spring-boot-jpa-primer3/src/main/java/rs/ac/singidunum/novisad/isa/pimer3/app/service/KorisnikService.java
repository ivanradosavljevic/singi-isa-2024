package rs.ac.singidunum.novisad.isa.pimer3.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rs.ac.singidunum.novisad.isa.pimer3.app.model.Korisnik;
import rs.ac.singidunum.novisad.isa.pimer3.app.model.Racun;
import rs.ac.singidunum.novisad.isa.pimer3.app.repository.KorisnikRepository;

@Service
public class KorisnikService {
	@Autowired
	private KorisnikRepository korisnikRepository;
	
	@Autowired
	private RacunService racunService;
	
	public Iterable<Korisnik> findAll() {
		return this.korisnikRepository.findAll();
	}
	
	public Optional<Korisnik> findById(Long id) {
		return this.korisnikRepository.findById(id);
	}
	
	@Transactional
	public Korisnik save(Korisnik korisnik) throws Exception {
		Korisnik noviKorisnik = this.korisnikRepository.save(korisnik);
		
		if(this.racunService.findOne(korisnik.getIme()).isPresent()) {
			throw new RuntimeException("Racun vec postoji!");
		}
		this.racunService.save(new Racun(noviKorisnik, noviKorisnik.getIme(), 0, false, null));
		
		return noviKorisnik;
	}
	
	public void delete(Korisnik korisnik) {
		this.korisnikRepository.delete(korisnik);
	}
	
	public void delete(Long id) {
		this.korisnikRepository.deleteById(id);
	}
} 
