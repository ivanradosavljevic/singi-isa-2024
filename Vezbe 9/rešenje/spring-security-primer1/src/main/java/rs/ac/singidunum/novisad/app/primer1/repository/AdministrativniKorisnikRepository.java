package rs.ac.singidunum.novisad.app.primer1.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.novisad.app.primer1.model.AdministrativniKorisnik;

@Repository
public interface AdministrativniKorisnikRepository extends CrudRepository<AdministrativniKorisnik, Long> {
	Optional<AdministrativniKorisnik> findByKorisnickoIme(String korisnickoIme);
}
