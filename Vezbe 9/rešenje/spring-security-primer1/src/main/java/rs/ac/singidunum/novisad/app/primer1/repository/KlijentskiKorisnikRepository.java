package rs.ac.singidunum.novisad.app.primer1.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.novisad.app.primer1.model.KlijentskiKorisnik;

@Repository
public interface KlijentskiKorisnikRepository extends CrudRepository<KlijentskiKorisnik, Long> {

}
