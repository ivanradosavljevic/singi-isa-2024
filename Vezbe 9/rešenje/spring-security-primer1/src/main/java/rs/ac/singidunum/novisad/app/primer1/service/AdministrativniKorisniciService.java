package rs.ac.singidunum.novisad.app.primer1.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.primer1.model.AdministrativniKorisnik;
import rs.ac.singidunum.novisad.app.primer1.repository.AdministrativniKorisnikRepository;

@Service
public class AdministrativniKorisniciService {
	@Autowired
	AdministrativniKorisnikRepository administrativniKorisniciRepository;

	public Iterable<AdministrativniKorisnik> findAll() {
		return this.administrativniKorisniciRepository.findAll();
	}
	
	public Optional<AdministrativniKorisnik> findByKorisnickoIme(String korisnickoIme) {
		return this.administrativniKorisniciRepository.findByKorisnickoIme(korisnickoIme);
	}
}
