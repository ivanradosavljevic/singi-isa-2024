package rs.ac.singidunum.novisad.app.primer1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.primer1.model.KlijentskiKorisnik;
import rs.ac.singidunum.novisad.app.primer1.repository.KlijentskiKorisnikRepository;

@Service
public class KlijentskiKorisnikService {
	@Autowired
	private KlijentskiKorisnikRepository klijentskiKorisnikRepository;
	
	public Iterable<KlijentskiKorisnik> findAll() {
		return klijentskiKorisnikRepository.findAll();
	}
}
