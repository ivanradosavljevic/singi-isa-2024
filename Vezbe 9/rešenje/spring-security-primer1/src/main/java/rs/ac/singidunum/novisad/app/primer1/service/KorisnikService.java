package rs.ac.singidunum.novisad.app.primer1.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.novisad.app.primer1.model.Korisnik;
import rs.ac.singidunum.novisad.app.primer1.repository.KorisnikRepository;

@Service
public class KorisnikService {
	@Autowired
	private KorisnikRepository korisnikRepository;
	
	public Iterable<Korisnik> findAll() {
		return korisnikRepository.findAll();
	}
	
	public Optional<Korisnik> findById(Long id) {
		return korisnikRepository.findById(id);
	}
}
